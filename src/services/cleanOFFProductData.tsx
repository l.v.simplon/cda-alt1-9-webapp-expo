import CleanedProductData from '../types/CleanedProductData';
import OFFProduct from '../types/OFFProduct';

export default (product: OFFProduct): CleanedProductData => ({
  id: product._id,
  name: product.abbreviated_product_name || product.product_name || product.generic_name,
  imageUrl: product.image_url,
  energyKcal100g: Number(product.nutriments["energy-kcal_100g"]),
  defaultServingGrams: Number(product.serving_quantity),
});
