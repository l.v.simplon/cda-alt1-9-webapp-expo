import { useEffect, useCallback } from "react";

// https://blog.webdevsimplified.com/2020-10/react-debounce/
const useDebouncedEffect = (effect: Function, deps: any[], delay = 250) => {
  const callback = useCallback(effect, deps)

  useEffect(() => {
    const timeout = setTimeout(callback, delay)
    return () => clearTimeout(timeout)
  }, [callback, delay])
}

export default useDebouncedEffect;
