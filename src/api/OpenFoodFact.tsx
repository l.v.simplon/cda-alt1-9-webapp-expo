import OFFProduct from '../types/OFFProduct';
type ApiResponse = {
  code: string,
  product: OFFProduct,
  status: number,
  status_verbose: string
}

export const getProductByCode = async (code: string): Promise<ApiResponse> => {
    const res = await fetch(`https://world.openfoodfacts.org/api/v0/product/${code}.json`);

    return await res.json();
}


type ApiSearchResponse = {
  products: OFFProduct[],
  count: number,
  page: number,
  page_count: number,
  page_size: number,
  skip: number,
}

export const searchProductByName = async (search: string, page = 1, perPage = 5): Promise<ApiSearchResponse> => {
    const res = await fetch(`https://world.openfoodfacts.org/cgi/search.pl?search_terms=${encodeURIComponent(search)}&json=1&page_size=${perPage}`);

    return await res.json();
}
