import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, StyleSheet, Button, Image } from 'react-native';

import TextInput from '../components/TextInputOneLine';
import OFFFood from '../components/OFFFood';
import { searchProductByName } from '../api/OpenFoodFact';
import useDebouncedAsyncTryCatch from '../hooks/useDebouncedAsyncTryCatch';

const ResultsList = ({isLoading, isError, result}) => {
  if (isLoading) return <View><Text>Loading...</Text></View>;
  // TODO (lol) display error message
  if (isError) return <View><Text>Error searching</Text></View>;

  return <ScrollView>
    { 
      // TODO: cache data when searching, this is going to refetch everything
      // make a repository that handles cache and fetching instead of managing it in a hook
      result.products.map(product => <OFFFood code={ product._id } /> )
    }
  </ScrollView>
}

export default () => {
  const [search, setSearch] = useState("");

  const {isLoading, isError, result} = useDebouncedAsyncTryCatch(
    () => searchProductByName(search),
    [search],
    600
  );


  return (
    <View>
      <View>
        <TextInput
          value={ search }
          onChangeText={ setSearch }
          placeholder="nutella"
        />
      </View>
      <ResultsList isError={isError} isLoading={isLoading} result={result} />
    </View>
  );
}
