
// Only typed the parts that interest me
type OFFProduct = {
  _id: string,
  abbreviated_product_name: string,
  product_name: string,
  generic_name: string,
  image_url: string,
  nutriments: {
    ["energy-kcal_100g"]: string | number,
  },
  serving_quantity: string | number,
};

export default OFFProduct;