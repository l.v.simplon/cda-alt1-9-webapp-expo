type CleanedProductData = {
  id: string,
  name: string,
  imageUrl: string,
  energyKcal100g: number,
  defaultServingGrams: number,
};

export default CleanedProductData;
