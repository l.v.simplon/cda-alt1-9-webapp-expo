export enum Routes {
  CALENDAR = "Calendar",
  SCANNER = "Scanner",
  SEARCH = "Search",
  ENTRIESDAY = "EntriesDay",
};

export type stackTyping = {
  [Routes.CALENDAR]: undefined;
  [Routes.SCANNER]: undefined;
  [Routes.SEARCH]: undefined;
  [Routes.ENTRIESDAY]: undefined;
};
